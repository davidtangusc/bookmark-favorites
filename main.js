var bookmarks = [
	{ 
		url: 'http://facebook.com',
		name: 'Facebook'
	},
	{ 
		url: 'http://espn.com',
		name: 'ESPN'
	},
	{ 
		url: 'http://javascriptweekly.com/',
		name: 'JavaScript Weekly'
	}
];

var bookmarkValidation = {
	hasValidUrl: function(url) {
		return /^https?:\/\/.+$/.test(url);
	},

	isNotEmpty: function(val) {
		if (val.length > 0) {
			return true;
		}

		return false;
	},

	passes: function(bookmark) {
		if (this.hasValidUrl(bookmark.url) && this.isNotEmpty(bookmark.name)) {
			return true;
		}

		return false;
	}
};


var bookmarkList = {
	$bookmarks: $('#bookmarks'),

	createBookmarkHtml: function(bookmark) {
		var bookmarkHtml = '<div>';
		bookmarkHtml += '<a href="' + bookmark.url + '">' + bookmark.name + '</a>';
		bookmarkHtml += '</div>';

		return bookmarkHtml;
	},

	render: function(bookmarks) {
		var bookmarkHtml;

		for (var i = 0; i < bookmarks.length; i++) {
			bookmarkHtml = this.createBookmarkHtml(bookmarks[i]);
			this.$bookmarks.append(bookmarkHtml);
		}
	},

	addOne: function(bookmark) {
		var bookmarkHtml = this.createBookmarkHtml(bookmark);
		this.$bookmarks.append(bookmarkHtml);
	}
};

bookmarkList.render(bookmarks);

$('#bookmark-form').on('submit', function(e) {
	var $this = $(this);
	e.preventDefault();

	var newBookmark = {
		url: document.getElementById('url').value,
		name: document.getElementById('name').value
	};

	var validation = bookmarkValidation.passes(newBookmark);

	if (validation) {
		bookmarkList.addOne(newBookmark);
		$this.find('input[type=text]').val('');
		$this.find('.error').hide();
	} else {
		$this.find('.error').show()
	}	
});